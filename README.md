# Bookshelf BNF API
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wearelautre_bookshelf-bnf-api&metric=alert_status)](https://sonarcloud.io/dashboard?id=wearelautre_bookshelf-bnf-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=wearelautre_bookshelf-bnf-api&metric=coverage)](https://sonarcloud.io/dashboard?id=wearelautre_bookshelf-bnf-api)

# Tools
## MVNW
### Update
`mvn -N io.takari:maven:0.7.6:wrapper`

## Dockerfile-maven (maven plugins)
[usage](https://github.com/spotify/dockerfile-maven/blob/master/docs/usage.md) 

# Build
`clean install -DskipTests -Ddockerfile.skip=true`

# Environment var

## Covers storage
- __COVER_SEARCH_PATH__ : Folder where the covers waiting to be saved will be stored
