openapi: "3.0.3"

info:
  description: "TODO"
  version: "1.0.0"
  title: "Swagger bookshelf"
  termsOfService: "http://swagger.io/terms/"
  contact:
    email: "gestin.gweltaz@protonmail.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"

tags:
  - name: "search"
    description: "Search book with ISBN"

schemes:
  - https
  - http

consumes:
  - application/json
produces:
  - application/json

paths:
  /search/books/findByISBN:
    get:
      consumes: []
      tags:
        - "search"
      summary: "Find book information by ISBN"
      description: "Find all book's informations corresponding to the ISBN"
      operationId: "findBooksByISBN"
      parameters:
        - $ref: "#/components/parameters/isbnParam"
      responses:
        200:
          $ref: '#/components/responses/SingleBookSuccess'
        400:
          description: "Invalid ISBN"
        404:
          description: "No book founded for the ISBN"
  /version:
    get:
      tags:
        - "technical"
      summary: "get app version informations"
      description: "Get application's version informations, with pipeline and job ids and commit reference "
      operationId: "getVersionInformation"
      responses:
        200:
          $ref: '#/components/responses/VersionInformationSuccess'
components:
  schemas:
    Book:
      type: object
      description: "Contains book information return after a search on from BNF API"
      required:
        - "title"
        - "editor"
      properties:
        isbn:
          type: string
          pattern: '^\d{10}(\d{3})?$'
          example: "9791026811336"
          description: "The unique book's identifier"
        title:
          type: string
          example: "Promethea"
          description: "The title of the book"
        subTitle:
          type: string
          example: "Livre Deuxième"
          description: "The sub title of the book"
        tome:
          type: string
          example: "5"
          description: "The book's rank in the series (issue number)"
        year:
          type: string
          example: "2019"
          description: "The book's publication year"
        collection:
          type: string
          example: "Collection DC rebirth"
          description: "The collection in which the book as been released in"
        editor:
          type: string
          example: "Urban comics"
          description: "The editor's name which has released the book"
        authors:
          type: array
          items:
            $ref: "#/components/schemas/Author"
          description: "The list of all retrieved authors whose work on the book"
        arkId:
          type: string
          example: "ark:/12148/cb45704173t"
          description: "An SRU ID use to link the book object to other in the API"
        cover:
          type: string
          example: "9791026811336"
          description: "The filename of the cover file (isbn)"
        rolesByAuthors:
          type: array
          description: "Raw data from the BNF SRU return to check and match the authors and roles"
          items:
            type: string
            example: "scénario, Mark Millar"
        series:
          type: string
          example: "Aquaman Rebith"
          description: "The book's series name"
    Author:
      type: object
      description: "Contains authors information related to a particular book, from BNF API"
      properties:
        role:
          type: array
          items:
            type: string
            example: "Scenario"
          description: "The list of all the role he had for a particular book"
        name:
          type: string
          example: "Scott Snyder"
          description: "The authors name"
    VersionInformation:
      type: object
      description: "Contains build and version of the running app instance"
      properties:
        projectVersion:
          type: string
          example: "0.0.1-SNAPSHOT"
          description: "The application version"
        pipelineId:
          type: string
          example: "235147613"
          description: "The pipeline id which build the current app version"
        jobId:
          type: string
          example: "934890028"
          description: "The job id which build the current app version"
        commitSha:
          type: string
          example: "954e7dd9a78057bff05c15903edb78e4db335043"
          description: "The commit on which the current app version"
  parameters:
    isbnParam:
      in: "query"
      name: "isbn"
      description: "The book's isbn of the book we want to retrieve"
      pattern: '^\d{13}?$'
      required: true
      schema:
        type: string
  responses:
    SingleBookSuccess:
      description: "A Book"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/Book"
    VersionInformationSuccess:
      description: "The Application version informations"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/VersionInformation"
