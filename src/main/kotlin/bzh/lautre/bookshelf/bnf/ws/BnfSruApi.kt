package bzh.lautre.bookshelf.bnf.ws

import bzh.lautre.bookshelf.bnf.ws.Helpers.getSetter
import bzh.lautre.bookshelf.bnf.ws.Helpers.mapToContentList
import bzh.lautre.bookshelf.bnf.ws.model.AuthorSearchResult
import bzh.lautre.bookshelf.bnf.ws.model.BookSearchResult
import org.apache.commons.lang3.tuple.Pair
import org.apache.http.client.fluent.Request
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.w3c.dom.Document
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.InvocationTargetException
import java.util.*
import java.util.regex.Pattern
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.reflect.KMutableProperty


@Component
class BnfSruApi(private val xPathAnalyser: XPathAnalyser) {

    private val logger = LoggerFactory.getLogger(BnfSruApi::class.java)

    @Value("\${covers.search.path}")
    private var coversSearchPath: String? = null

    fun getBookDataByIsbn(isbn: String): Optional<BookSearchResult> {

        val url = "http://catalogue.bnf.fr/api/SRU?" +
                "version=1.2&" +
                "operation=searchRetrieve&" +
                "query=bib.isbn%20all%20%22" +
                isbn +
                "%22&" +
                "recordSchema=intermarcxchange&" +
                "maximumRecords=20&" +
                "startRecord=1"

        var bookSearchResult: BookSearchResult? = null

        try {
            val dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
            val doc = dBuilder.parse(Request.Get(url)
                    .connectTimeout(1000)
                    .socketTimeout(1000)
                    .execute()
                    .returnContent()
                    .asStream())

            val retrievedIsbn = sanitizeIsbn(xPathAnalyser.getValueStringByPair(doc, ISBN))

            if (retrievedIsbn.isNotEmpty()) {
                bookSearchResult = BookSearchResult()
                bookSearchResult.isbn = retrievedIsbn
                bookSearchResult.arkId = xPathAnalyser.getValueString(doc, listOf(XPATH_EXP_ID))
                bookSearchResult.collection = xPathAnalyser.getValueStringByPair(doc, COLLECTION)
                bookSearchResult.editor = xPathAnalyser.getValueStringByPair(doc, EDITOR)
                bookSearchResult.series = xPathAnalyser.getValueStringByPair(doc, SERIES)
                bookSearchResult.title = xPathAnalyser.getValueStringByPair(doc, TITLE)
                bookSearchResult.subTitle = xPathAnalyser.getValueStringByPair(doc, SUBTITLE)
                bookSearchResult.tome = sanitizeTome(xPathAnalyser.getValueStringByPair(doc, TOME))
                bookSearchResult.year = xPathAnalyser.getValueStringByPair(doc, YEAR)
                bookSearchResult.rolesByAuthors = mapToContentList(xPathAnalyser.getNodeList(doc, AUTHORS_WITH_ROLE))
                bookSearchResult.authors = getAuthors(doc, bookSearchResult.rolesByAuthors!!)

                if (bookSearchResult.tome === null || bookSearchResult.series === "") {
                    bookSearchResult.series = bookSearchResult.title
                }

                this.getBookCover(bookSearchResult)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return Optional.ofNullable(bookSearchResult)
    }

    private fun getBookCover(bookSearchResult: BookSearchResult) {

        val url = """https://catalogue.bnf.fr/couverture?&appName=NE&idArk=${bookSearchResult.arkId}&couverture=1"""

        val directory = File(coversSearchPath!!)
        if (!directory.exists()) {
            directory.mkdirs()
            directory.setWritable(true)
            directory.setReadable(true)
        }

        val coverPath = """${this.coversSearchPath}/${bookSearchResult.isbn!!.replace("-", "")}.jpg"""
        logger.info(coverPath)
        try {
            val fos = FileOutputStream(coverPath)
            fos.write(Request.Get(url).connectTimeout(1000).socketTimeout(1000).execute().returnContent().asBytes())
            fos.close()

            if (!ImageHelper.isNoCoverImage(File(coverPath))) {
                bookSearchResult.cover = File(coverPath).name
                File(coverPath).setReadable(true, false)
                File(coverPath).setWritable(true, false)
                logger.info("Cover available through BFN API")
            } else {
                logger.error("Cover not available through BFN API")
                throw Exception("No cover available")
            }
        } catch (e: Exception) {
            logger.error(e.message)
            File(coverPath).delete()
            bookSearchResult.cover = null
        }
    }

    private fun getAuthors(doc: Document, rolesAndAuthorsList: List<String>): List<AuthorSearchResult> {

        val authorSearchResultList = ArrayList<AuthorSearchResult>()
        AUTHORS_INFORMATIONS.forEach { s, pairList ->
            try {
                updateAuthorsList(authorSearchResultList, mapToContentList(xPathAnalyser.getNodeList(doc, pairList)), getSetter(s))
            } catch (e: ClassNotFoundException) {
                e.printStackTrace()
            }
        }

        authorSearchResultList.forEach { authorSearchResult -> addRoles(rolesAndAuthorsList, authorSearchResult) }
        return authorSearchResultList
    }

    private fun updateAuthorsList(authorSearchResultList: MutableList<AuthorSearchResult>, authorsInfoList: List<String>, setter: KMutableProperty.Setter<*>) {
        for (i in authorsInfoList.indices) {
            // Get the private field
            val isPresent: Boolean = authorSearchResultList.size > i
            val authorSearchResult: AuthorSearchResult = if (isPresent) authorSearchResultList[i] else AuthorSearchResult()
            try {
                setter.call(authorSearchResult, authorsInfoList[i])
                if (!isPresent) {
                    authorSearchResultList.add(authorSearchResult)
                }
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
            }

        }
    }

    private fun addRoles(stringList: List<String>, authorSearchResult: AuthorSearchResult) {
        val rolesList = HashSet<String>()
        stringList
                .stream()
                .filter { string -> string.contains(authorSearchResult.name) }
                .forEach { s ->
                    val matcher = Pattern.compile(AUTHOR_ROLES_REGEX).matcher(s)
                    if (matcher.find()) {
                        for (i in 1..matcher.groupCount()) {
                            rolesList.add(matcher.group(i))
                        }
                    }
                }
        authorSearchResult.role = rolesList
    }

    private fun sanitizeIsbn(isbn: String): String {
        return isbn.replace("-", "").replace(" ", "").trim { it <= ' ' }
    }

    private fun sanitizeTome(tome: String): Int? {
        val m = Pattern.compile("([\\d]+)").matcher(tome)
        return if (m.find()) Integer.parseInt(m.group(1)) else null
    }

    companion object {

        private const val XPATH_EXP_ID = "//@id"
        private val ISBN = listOf(Pair.of("020", "a"))
        private val EDITOR = listOf(Pair.of("260", "c"))
        private val COLLECTION = listOf(Pair.of("410", "t"))
        private val SERIES = listOf(Pair.of("460", "t"), Pair.of("295", "a"))
        private val TITLE = listOf(Pair.of("245", "i"), Pair.of("245", "a"))
        private val SUBTITLE = listOf(Pair.of("245", "h"))
        private val TOME = listOf(Pair.of("460", "v"), Pair.of("245", "h"))
        private val YEAR = listOf(Pair.of("260", "d"))

        private val AUTHORS_WITH_ROLE = listOf(Pair.of("245", "f"), Pair.of("245", "g"))
        private val AUTHORS_INFORMATIONS = object : HashMap<String, List<Pair<String, String>>>() {
            init {
                put("bnfId", listOf(Pair.of("100", "3"), Pair.of("700", "3")))
                put("firstname", listOf(Pair.of("100", "m"), Pair.of("700", "m")))
                put("lastname", listOf(Pair.of("100", "a"), Pair.of("700", "a")))
            }
        }

        private const val AUTHOR_ROLES_REGEX = "(dessins?|sc[é|e]narios?|encrages?|couleurs?|traductions?)"
    }
}
