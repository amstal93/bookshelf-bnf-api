package bzh.lautre.bookshelf.bnf.business.impl

import bzh.lautre.bookshelf.bnf.business.VersionService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class VersionServiceImpl: VersionService {

    @Value("\${project-version}")
    private val projectVersion: String = "Local"

    @Value("\${ci-pipeline-id}")
    private val pipelineId: String = "0"

    @Value("\${ci-job-id}")
    private val jobId: String = "0"

    @Value("\${commit-sha}")
    private val commitSha: String = "0"

    override fun getVersionInformation(): VersionService.VersionInformation {
        return VersionService.VersionInformation(projectVersion, pipelineId, jobId, commitSha)
    }
}
