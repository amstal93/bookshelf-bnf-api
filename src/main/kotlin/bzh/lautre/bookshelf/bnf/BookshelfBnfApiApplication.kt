package bzh.lautre.bookshelf.bnf

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.PropertySource

@PropertySource("classpath:versions.properties")
@SpringBootApplication(scanBasePackages = ["bzh.lautre.bookshelf.bnf"])
class BookshelfBnfApiApplication : SpringBootServletInitializer()

fun main(args: Array<String>) {
	runApplication<BookshelfBnfApiApplication>(*args)
}
